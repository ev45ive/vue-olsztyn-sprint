import { AuthService } from "./services/AuthService";

export const authService = new AuthService(
  "https://accounts.spotify.com/authorize",
  {
    client_id: "70599ee5812a4a16abd861625a38f5a6",
    redirect_uri: "http://localhost:8080/",
    response_type: "token"
  }
);
// https://developer.spotify.com/
// placki@placki.com
// ******

import axios, { AxiosError, AxiosResponse } from "axios";
import { MusicSearchService } from "./services/MusicSearch";
// axios.defaults.headers['Authorization'] = authService.getToken()

axios.interceptors.request.use(req => {
  req.headers["Authorization"] =
    //
    `Bearer ${authService.getToken()}`;

  return req;
});

axios.interceptors.response.use(
  req => req,
  (err: AxiosError) => {
    
    if (err.response && err.response['status'] == 401) {
      authService.authorize();
    }
    throw err;
  }
);

export const musicSearch = new MusicSearchService();
