import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./registerServiceWorker";

import "bootstrap/dist/css/bootstrap.css";
import SearchField from './components/SearchField.vue';
import Card from './components/Card.vue';

Vue.config.productionTip = false;

declare module "vue/types/vue" {
  interface Vue {
    // $style: Readonly<Record<string, {}>>;
    $style: {
      readonly [key: string]: string;
    };
    
  }
}

/* Register component globally */
Vue.component('SearchField',SearchField)
Vue.component('Card',Card)

new Vue({
  // el:'#app',
  // template:'<div>placki</div>',
  router,
  store,
  render: h => h(App)
}).$mount("#app");

// VirtualDOM
// h('div',{id:zmienna},h('p',{},'ala ma '+animal),h(Component)),

// <div id="123"><p>Ala ma {{animal}}</p></div>
