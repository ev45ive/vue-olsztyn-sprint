import Vue from "vue";
import Vuex from "vuex";
import { counter } from "./modules/counter";
import { playlists } from "./modules/playlists";
import Music from "./modules/music";

Vue.use(Vuex);

export interface AppState {}

export default new Vuex.Store<AppState>({
  modules: {
    counter,
    playlists,
    music: Music
  }
});
