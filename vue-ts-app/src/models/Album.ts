interface Entity {
  id: string;
  name: string;
}

export interface Album extends Entity {
  artists: Artist[];
  images: AlbumImage[];
}

export interface Artist extends Entity {
  popularity: number;
}

export interface AlbumImage {
  url: string;
  width: string;
  height: string;
}

export interface PageObject<T> {
  items: T[];
  offset: number;
  total: number;
}

export interface AlbumsResponse {
  albums: PageObject<Album>;
}
export interface ArtistsResponse {
  artists: PageObject<Artist>;
}
