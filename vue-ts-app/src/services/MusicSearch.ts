import axios from "axios";
import { AlbumsResponse, Album } from "../models/Album";
import { SearchResult } from "../models/SearchResult";

export const MusicServiceToken = Symbol('MusicService Injection Token')

export class MusicSearchService {
  api_url = "https://api.spotify.com/v1/search";

  searchAlbums(query: string) {
    return axios
      .get<AlbumsResponse>(this.api_url, {
        params: {
          type: "album",
          q: query
        },
        headers: {
          Authorization: ""
        }
      })
      .then(resp => resp.data.albums.items)
      .then(results =>
        results.map(({ id, name, images }) => ({
          id,
          name,
          image: images[0].url
        }))
      )
      .catch(err => {
        console.error(err);
        // return Promise.reject(err)
        return [];
      });
  }
}

// XMLHttpRequest()

// fetch().then(resp => resp.json()).then...

// axios.get().then(result => result.data )

// Vue Resource
// this.$http.get()

//https://www.npmjs.com/package/vue-axios
