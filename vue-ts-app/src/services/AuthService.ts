export class AuthService {
  // private auth_url: string,

  constructor(
    private auth_url: string,
    private options: {
      client_id: string;
      response_type: string;
      redirect_uri: string;
    }
  ) {
    const token = sessionStorage.getItem("token");
    if (token) {
      this.token = JSON.parse(token);
    }

    if (location.hash) {
      const match = location.hash.match(/access_token=([^&]+)/);
      this.token = match && match[1];
      sessionStorage.setItem("token", JSON.stringify(this.token));
    }
  }

  private token: string | null = null;

  authorize() {
    const { client_id, response_type, redirect_uri } = this.options;

    const url =
      `${this.auth_url}?` +
      `client_id=${client_id}&` +
      `response_type=${response_type}&` +
      `redirect_uri=${redirect_uri}&state=placki`;

    sessionStorage.removeItem("token");
    location.href = url;
  }

  getToken() {
    if (!this.token) {
      this.authorize();
    }

    if (this.token) {
      return this.token;
    }
  }
}
