import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import Playlists from "./views/playlists/Playlists.vue";
import PlaylistDetails from "./views/playlists/components/PlaylistDetails.vue";
import PlaylistForm from "./views/playlists/components/PlaylistForm.vue";
import SelectedDetails from './views/playlists/SelectedDetails.vue';

Vue.use(Router);

export default new Router({
  // fallback: true,
  mode: "history",
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
      // beforeEnter(to, from,next){ next(null)}
    },
    {
      path: "/playlists",
      component: Playlists,
      children: [
        {
          path: "",
          name: "playlists",
          component: {
            render(h) {
              return h("p", {}, "Please select");
            }
          }
        },
        {
          path: ":id",
          component: SelectedDetails
        },
        {
          path: ":id/edit",
          component: PlaylistForm
        }
      ]
    },
    {
      path: "/search",
      name: "search",
      component: () =>
        import(/* webpackChunkName: "search" */ "./views/search/MusicSearch.vue")
    },
    {
      path: "/about",
      name: "about",
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "about" */ "./views/About.vue")
    },
    {
      // will match everything
      path: "*",
      redirect: {
        name: "about"
      }
      // component: {
      //   render(h) {
      //     return h("h1", {}, "404");
      //   }
      // }
    }
  ]
});
