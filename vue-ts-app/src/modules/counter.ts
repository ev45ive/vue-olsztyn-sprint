import { Module } from "vuex";
import { AppState } from "../store";

export const counter: Module<
  {
    counter: number;
  },
  AppState
> = {
  namespaced: true,
  state: {
    counter: 1
  },
  mutations: {
    inc(s, payload = 1) {
      s.counter += payload;
    },
    dec(s, payload = 1) {
      s.counter -= payload;
    }
  },
  actions: {
    increment({ commit, rootState }, payload) {
      commit("inc", payload);
    },
    decrement({ commit }, payload) {
      commit("dec", payload);
    }
  },
  getters: {
    counter(s, getters) {
      return s.counter;
    }
  }
};
