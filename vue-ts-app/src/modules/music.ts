import {
  Module,
  VuexModule,
  Mutation,
  Action,
  MutationAction
} from "vuex-module-decorators";
import { Album } from "../models/Album";
import { musicSearch } from '../services';

@Module({
  // name: "music", // no no! ;-(
  namespaced: true
})
export default class Music extends VuexModule {
  _albums: Album[] = [];
  _query: string = "";

  @Mutation
  searchStart(query: string) {
    this._query = query;
  }

  @Mutation
  searchSuccess(albums: Album[]) {
    this._albums = albums;
  }

  @Mutation
  searchError(albums: Album[]) {
    this._albums = albums;
  }

  @Action({ commit: "searchStart" })
  search(query: string) {
    musicSearch
      .searchAlbums(query)
      .then(albums => {
        this.context.commit("searchSuccess", albums);
      })
      .catch(error => {
        this.context.commit("searchError", error);
      });

    return query;
  }

  @Action({})
  async searchasync(query: string) {
    this.context.dispatch("startSearch", query);
    try {
      const albums = musicSearch.searchAlbums(query);

      this.context.commit("searchSuccess", await albums);
    } catch (error) {
      this.context.commit("searchError", error);
    }
  }

  @Action({})
  async searchIOC(query: string) {
    // musicSearch.searchAlbumsIOC(query, this.context.dispatch)
  }

  get results() {
    return this._albums;
  }
  get query() {
    return this._query;
  }
}
