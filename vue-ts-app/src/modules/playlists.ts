import { AppState } from "../store";
import { Module, Action } from "vuex";
import { Playlist } from "../models/Playlist";

interface PlaylistsState {
  // entities: { [key: number]: Playlist };
  // list: number[],
  items: Playlist[];
  selected: Playlist["id"] | null;
  query: string;
}
const LOADED = "LOADED";
const SET_QUERY = "SET_QUERY";
const UPDATE_PLAYLIST = "UPDATE_PLAYLIST";

export const playlists: Module<PlaylistsState, AppState> = {
  namespaced: true,
  state: {
    items: [
      {
        id: 123,
        name: "Vue Hits!",
        favorite: true,
        color: "#ff00ff"
      },
      {
        id: 234,
        name: "Vue TOP20",
        favorite: false,
        color: "#ffff00"
      },
      {
        id: 345,
        name: "Best of Vue!",
        favorite: true,
        color: "#00ffff"
      }
    ],
    selected: null,
    query: ""
  },
  mutations: {
    [LOADED](s, payload: Playlist[]) {
      s.items = payload;
    },
    selected(s, id: Playlist["id"]) {
      s.selected = id;
    },
    [SET_QUERY](s, query) {
      s.query = query;
    },
    [UPDATE_PLAYLIST](s, payload) {
      const index = s.items.findIndex(p => p.id == payload.id);
      if (index !== -1) {
        s.items.splice(index, 1, payload);
      }
    }
  },
  actions: {
    fetchPlaylists() {},

    select({ commit, state }, id: Playlist["id"] | null) {
      id = state.selected == id ? null : id;
      commit("selected", id);
    },

    search({ commit }, query: string) {
      commit(SET_QUERY, query);
    },

    save({ commit, state }, payload) {
      // Validate payload
      commit(UPDATE_PLAYLIST, payload);
    }
  },
  getters: {
    selected({ items, selected }) {
      return items.find(i => i.id == selected);
    },
    filtered({ items, query }) {
      return items.filter(i => i.name.includes(query));
    }
  }
};

// const save: Action<PlaylistsState, AppState> = ({ commit, state }, payload) => {
//   // Validate payload
//   commit(UPDATE_PLAYLIST, payload);
// };
